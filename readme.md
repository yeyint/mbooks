## Mmebooks Library
Instalation
=========
```sh
git clone  

composer install

```

Migration
=========
```sh
php artisan migrate --package=cartalyst/sentry
php artisan config:publish cartalyst/sentry
php artisan migrate
php artisan db:seed
```
Admin
======
Admin url can change in config 'app.backend_url'
```sh
<https://www.domain.com/backend> Default 

```

```sh
user - admin@example.com
pass - rootroot
```

Optional need to install chrome extension for Api
=================================================

<https://chrome.google.com/webstore/detail/json-viewer/gbmdgpbipfallnflgajpaliibnhdgobh?utm_source=chrome-app-launcher-info-dialog> JsonViewer


Api
=======

1. <http://www.domain.com/api/main>           All
2. <http://www.domain.com/api/category>       Categories
3. <http://www.domain.com/api/publisher>      Publishers
4. <http://www.domain.com/api/author>         Authors
5. <http://www.domain.com/api/editorpick>     Editorpicks

UPDATE (Related :author,tag,category)
