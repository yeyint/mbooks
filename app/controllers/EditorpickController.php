<?php

class EditorpickController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */


	public function index()
	{   
		$picks=Editorpick::all();
		return View::make('editorpick.index',with([
             'picks' => $picks,
             'book' => new Book,
             'author' => new Author,
			]));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('editorpick.create')->with([

           'book' => new Book,

			]);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$id=Input::get('book_id');
		$book=Book::find($id);
		
		$pick=Editorpick::create([
            'book_id' => $book->id,
            'book_title' => $book->title,
            'author_name'=> $book->author_id,
            'book_subjects' => $book->subjects,
			]);


		if ($pick->save()) {
			return $this->goToIndex(); 


		}else{
            
            return Redirect::back()->withInput()->withErrors($pick->getErrors());

		}



	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$pick=Editorpick::find($id);

		return View::make('editorpick.edit')->with([
            'pick' => $pick,
            'book' => new Book,
            ]
			);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		 Editorpick::find($id)->delete();

		return $this->goToIndex();
	}


	private function goToIndex() {
        return Redirect::route(Config::get('app.backend_url').'.editorpick.index');
    }


}
