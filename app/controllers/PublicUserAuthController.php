<?php

class PublicUserAuthController extends \BaseController {

	private $hashids;

	public function __construct() {
		$this->hashids = new Hashids\Hashids(Config::get('app.salt_for_public_value'), 16, Config::get('random_string'));
	}

	public function getAttemptActivation( $hashId, $activationCode )
	{
		// return [$hashId, $activationCode];
		// return [$this->hashids->encode(5)];
		try
		{
		    // Find the user using the user id
		    if (!isset($this->hashids->decode($hashId)[0])) {
		    	return $this->responseMissionRequest("Require parameters missing!");
		    }

		    $user = Sentry::findUserById($this->hashids->decode($hashId)[0]);
		    // dd($user);
		    // dd($this->hashids->decode($hashId)[0]);
		    // Attempt to activate the user
		    if ($user->attemptActivation($activationCode))
		    {
		    	$user['permissions'] = array(
		    	    'user' => 1
		    	);
		    	$user->save();
		        return $this->responseActivationSuccess($user);
		    }
		    else
		    {
		        return $this->responseActivationFailed("Activation Failed");
		    }
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    return $this->responseActivationFailed("User was not found.");
		}
		catch (Cartalyst\Sentry\Users\UserAlreadyActivatedException $e)
		{
		    return $this->responseActivationFailed("User is already activated.");
		}
		return $this->responseActivationFailed("Activation Failed");
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getLogin()
	{
	    return View::make('frontend.user.login');
	}

	public function apiAuth()
	{
		// dd($this->isValidRequest());
		// dd(Input::all());
		if (!$this->isValidRequest()) {
			return $this->responseMissionRequest("Require parameter 'tag' is missing!");
		}

		switch (Input::get('tag')) {
			case 'login':
				return $this->postLogin();
				break;
			case 'register':
				return $this->postRegister();
				break;
			default:
				return $this->postLogin();
				break;
		}
	}

	public function isValidRequest()
	{
		if(Input::has('tag') && !empty(Input::get('tag'))) {
			return true;
		}
		return false;
	}

	public function postLogin()
	{
		try
        {
            $credentials = array(
                'email'    => Input::get('email'),
                'password' => Input::get('password'),
            );
            $user = Sentry::authenticate($credentials, false);
        }
        catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
        {
            return $this->responseLoginError("Login field is required.");
        }
        catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
        {
            return $this->responseLoginError("Password field is required.");
        }
        catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
        {
            return $this->responseLoginError("Wrong password, try again.");
        }
        catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
        {
            return $this->responseLoginError("User was not found.");
        }
        catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
        {
            return $this->responseLoginError("User is not activated.");
        }

        catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
        {
            return $this->responseLoginError("User is suspended.");
        }
        catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
        {
            return $this->responseLoginError("User is banned.");
        }
        return $this->responseLoginSuccess($user);
	}

	public function postRegister()
	{
		try
		{
		    // Let's register a user.
		    $user = Sentry::createUser(array(
				'first_name' => Input::get("name"),
				'email'      => Input::get("email"),
				'password'   => Input::get("password"),
                'activated'  => true
		    ));

		    // Let's get the activation code
		    // $activationCode = $user->getActivationCode();

		    // Send activation code to the user so he can activate the account
		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
		    return $this->responseRegisterError('Login field is required.');
		}
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
		{
		    return $this->responseRegisterError('Password field is required.');
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
		    return $this->responseRegisterUserAlreadyExistedError('User with this login already exists.');
		}
		// $this->sendActivationMail($user->email, $this->hashids->encode($user->id), $activationCode);
		return $this->responseRegisterSuccess($user);
	}

	/**
	 * Login success
	 * @param  [type] $user [description]
	 * @return [type]       [description]
	 */
	public function responseLoginSuccess( $user )
	{
		return [
			'tag'     => 'login',
			'success' => true,
			'error'   => false,
			'uid'     => $this->getUUID($user->id),
			'user'    => [
				'name'       => $user->first_name . ' ' . $user->last_name,
				'email'      => $user->email,
				'created_at' => $user->created_at,
				'updated_at' => $user->updated_at,
			], 
		];
	}

	/**
	 * Login error - Incorrect username/password
	 * @param  [type] $errorMessage [description]
	 * @return [type]               [description]
	 */
	public function responseLoginError($errorMessage)
	{
		return [
			'tag'     => 'login',
			'success' => false,
			'error'   => true,
			'error_msg'   => $errorMessage,
		];
	}

	/**
	 * Registeration success response (User successfully stored)
	 * @param  [type] $user [description]
	 * @return [type]       [description]
	 */
	public function responseRegisterSuccess( $user )
	{
		return [
			'tag'     => 'register',
			'error'   => false,
			'uid'     => $this->getUUID($user->id),
			'user'    => [
							'name'       => $user->first_name . ' ' . $user->last_name,
							'email'      => $user->email,
							'created_at' => $user->created_at,
							'updated_at' => $user->updated_at,
						]
		];
	}

	/**
	 * Register error in storing
	 * @param  [type] $errorMessage [description]
	 * @return [type]               [description]
	 */
	public function responseRegisterError($errorMessage)
	{
		return [
			'tag'     => 'register',
			'success' => false,
			'error'   => true,
			'error_msg'   => $errorMessage,
		];
	}

	/**
	 * Register error - User Already Existed
	 * @param  [type] $errorMessage [description]
	 * @return [type]               [description]
	 */
	public function responseRegisterUserAlreadyExistedError($errorMessage)
	{
		return [
			'tag'     => 'register',
			'success' => false,
			'error'   => true,
			'error_msg'   => $errorMessage,
		];
	}

	/**
	 * Response for Missing Request
	 * @return
	 */
	public function responseMissionRequest($errorMessage)
	{
		return [
			'success' => false,
			'error'   => true,
			'error_msg'   => $errorMessage,
		];
	}

	public function sendActivationMail( $to,$userId,  $activationCode )
	{
		Config::set('mbooks.temp.activation.email', $to);
		// Config::set('mbooks.temp.activation.userId', $userId);
		Mail::send('email.activation', array(
			'activationLink' => URL::route("frontend.getAttemptActivation", [$userId, $activationCode]),
			// 'userId' => $userId,
			), function($message)
		{
		    $message->to(Config::get('mbooks.temp.activation.email'), '')->subject('Account Activation');
		});
	}

	public function getUUID($userId)
	{
		return $this->hashids->encode($userId);
	}

	////////////////// Activation ///////////////
	public function responseActivationSuccess( $user )
	{
		return View::make('pages.plain')->with([
			'message' => "Your subscription was successfully activated. Thank you.",
			'alert_message' => "Your subscription was successfully activated. Thank you.",
			'close' => true,
		]);
		// return [
		// 			'tag'     => 'activate',
		// 			'success' => 1,
		// 			'error'   => 0,
		// 			'uid'     => $this->getUUID($user->id),
		// 			'user'    => [
		// 				'name'       => $user->first_name . ' ' . $user->last_name,
		// 				'email'      => $user->email,
		// 				'created_at' => null,
		// 				'updated_at' => null,
		// 			], 
		// 		];
	}

	public function responseActivationFailed( $errorMessage )
	{
		// return [
		// 	'tag'     => 'activate',
		// 	'success' => 0,
		// 	'error'   => 3,
		// 	'error_msg'   => $errorMessage,
		// ];
		return View::make('pages.plain')->with([
			'message' => $errorMessage,
			'alert_message' => $errorMessage,
			'close' => false
			]);
	}
}
