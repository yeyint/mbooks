<?php

class EditorpickApiController extends \BaseController {

	public function get ($wrap = true)
    {
        return $this->response($this->index(), $wrap);
        // $id = Input::get('id');
        // if ($id === null) {
        // }
        // return $this->show($id);
    }


    public function response($editorpick, $wrap) {
        if ($wrap)
            return [Config::get('api.editorpick_wrapper') => $editorpick];
        else 
            return $editorpick;
    }

    public function index()
    {
        $result = Editorpick::all()->toArray();
        $categories = [];
        foreach ($result as $key) {
            $categories[] = $this->transformer($key);
        }
        return $categories;
    }

    public function transformer($editorpick) {
        $api = Config::get('api.editorpick');
        $transformed = [];
        foreach ($api as $key => $value) {
            $transformed[$value] = (isset($editorpick[$key]) && $editorpick[$key] ) ? $editorpick[$key]: '';
        }
        return $transformed;
    }

}
