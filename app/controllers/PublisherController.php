<?php

class PublisherController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
$publishers = Publisher::orderBy('id', 'DESC')->get()->toArray();
		return View::make('publisher.index')->with(compact('publishers'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('publisher.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		
		$name = Input::get('name');
		$exists = Publisher::where('name', $name)->first();
		if (!$exists) {
			$cat = Publisher::create([
				'name' => $name,
				'address' => Input::get('address'),
				'phone' => Input::get('phone_no'),
		
			]);
			if ($cat->save()) {
				return Redirect::route(Config::get('app.backend_url'). '.publisher.index');
			} else {

                return Redirect::back()->withInput()->withErrors($cat->getErrors());

			}
		} else {
			echo ('category is already exists');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$publisher=Publisher::find($id);
	    return View::make('publisher.edit')->with(compact('publisher'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$publisher = Publisher::find($id)
						->update([
							'name' => Input::get('name'),
							'address' => Input::get('address'),
							'phone' => Input::get('phone_no'),
						]);
		return $this->goToIndex();
	}


    public function destroy($id)
	{
		 Publisher::find($id)->delete();

		return $this->goToIndex();
	}

	private function goToIndex() {
		return Redirect::route(Config::get("app.backend_url"). '.publisher.index');
	}


}
