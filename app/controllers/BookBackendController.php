<?php

class BookBackendController extends \BaseController {

	// app/models/book
	private $model;

	private $hashids;

	public function __construct() {
		$this->model = new Book;
		$this->hashids = new Hashids\Hashids(Config::get('app.salt_value'), 8, Config::get('random_string'));
	}
    
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$books = Book::orderBy('id', 'DESC')->paginate(20);

		return View::make('book.index')->with([
				'books'   => $books,
				'author'   => new Author,
				'category' => new Category,
			]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('book.create')->with([
				'author'   => new Author,
				'category' => new Category,
				'publisher' => new Publisher,
			]);
	}


	public function store()
	{   



        $destinationPath=$this->getAvatorPath();
        $cover_pic=null;
        $share_pic=null;

        if (Input::hasFile('cover')) {
            $file            = Input::file('cover');
            $cover_pic        = str_random(4) . '_'. date('d_m_Y') .'.jpg';
            $uploadSuccess   = $file->move($destinationPath, $cover_pic);
            $path = $destinationPath . $cover_pic;
            $img = Image::make($path);
            $img->save($path);
            // return $img->response('jpg');
        }

        if (Input::hasFile('share_pic')) {
            $file            = Input::file('share_pic');
            $share_pic       = str_random(4) . '_'. date('d_m_Y') .'.jpg';
            $uploadSuccess   = $file->move($destinationPath, $share_pic);
            $path = $destinationPath . $share_pic;
            $img = Image::make($path);
            // ->resize(600, 480);
            $img->save($path);
            // return $img->response('jpg');
        }
      
         $is_notes=Input::has('is_notes') ? true :false;

		$success = Book::create([
			'title'     => Input::get('title'),
            'cover_pic' => $cover_pic,
            'share_pic' => $share_pic,
			'subjects'     => Input::get('subjects'),
			'category_id' => serialize(Input::get('categories')),
			'author_id'   => Input::get('author'),
			'share_link'  => '',
			'publisher_id'   => Input::get('publisher'),
			'is_notes'    => $is_notes,
			'notes'   => Input::get('notes'),
			'physical_description'   => Input::get('physical_description'),
			'isbn'   => Input::get('isbn'),
			'pages_no' => Input::get('pages_no'),
			]);


		if($success ->save()){
			$id = $success->id;
			$unique_id = $this->hashids->encode($id);
			$book = Book::find($id)->update([
					'unique_id' => $unique_id,
					'share_link' => Config::get('app.url') . '/q/' . $unique_id
				]);
			return $this->goToIndex();
		}else{

            return Redirect::back()->withInput()->withErrors($success->getErrors());
        }
		throw new Exception("Error: can't store book.", 1);
		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$book = Book::find($id)->toArray();
/*		if ($book['is_notes']== true) {
			$book['is_notes']=true;
		}else{
           $book['is_notes']=false;

		}*/
		$book['category_id']=unserialize($book['category_id']);
		return  View::make('book.edit')->with(
				[
				'book' => $book,
				'author' => new Author,
				'category' => new Category,
				'publisher' => new Publisher,
				]
			);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

        $destinationPath=$this->getAvatorPath();
        $cover_pic=null;
        $share_pic=null;

        $book=Book::find($id);
        if(!$book){

            throw new Exception('Book not found',1);

        }
/*        if(
            isset($book->cover_pic)
            && $book->cover_pic
            && is_file($destinationPath.$book->cover_pic)) {
            unlink( $destinationPath.$book->cover_pic );
        }*/

        if (Input::hasFile('cover')) {
            $file            = Input::file('cover');
            $cover_pic        = str_random(4) . '_'. date('d_m_Y') .'.jpg';
            $uploadSuccess   = $file->move($destinationPath, $cover_pic);
            $path = $destinationPath . $cover_pic;
            $img = Image::make($path);
            $img->save($path);
            // return $img->response('jpg');
        }elseif (!$book->cover_pic) {
        	
         $cover_pic=null;

        }else{

           $cover_pic=$book->cover_pic;
           $path = $destinationPath . $cover_pic;
           $img = Image::make($path);
           $img->save($path);

        }
        if (Input::hasFile('share_pic')) {
            $file            = Input::file('share_pic');
            $share_pic       = str_random(4) . '_'. date('d_m_Y') .'.jpg';
            $uploadSuccess   = $file->move($destinationPath, $share_pic);
            $path = $destinationPath . $share_pic;
            $img = Image::make($path);
            // ->resize(600, 480);
            $img->save($path);
            // return $img->response('jpg');
        }elseif (!$book->share_pic) {
        	
         $share_pic=null;

        }else{

           $cover_pic=$book->cover_pic;
           $path = $destinationPath . $cover_pic;
           $img = Image::make($path);
           $img->save($path);

        }

		// dd(Input::get());
        $is_notes=Input::has('is_notes') ? true :false;
        $share_link=Config::get('app.url') . '/q/' . $book->unique_id;
        
		$success = Book::find($id)->update([
            'title'                => Input::get('title'),
            'cover_pic'            => $cover_pic,
            'share_pic'            => $share_pic,
            'subjects'             => Input::get('subjects'),
            'category_id'          => serialize(Input::get('categories')),
            'author_id'            => Input::get('author'),
            'share_link'           => $share_link,
            'publisher_id'         => Input::get('publisher'),
            'is_notes'             => $is_notes,
            'notes'                => Input::get('notes'),
            'physical_description' => Input::get('physical_description'),
            'isbn'                 => Input::get('isbn'),
            'pages_no'             => Input::get('pages_no'),
			]);
		if ($success) {
			return $this->goToIndex();
		}else{

           return Redirect::back()->withInput()->withErrors($book->getErrors());
        }



	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		 Book::find($id)->delete();

		return $this->goToIndex();
	}

	private function goToIndex() {
        return Redirect::route(Config::get('app.backend_url').'.book.index');
    }

    public function generateRandomKey($length) {
        $random = '';
        for ($i = 0; $i < $length; $i++) {
        	$random .= rand(0,9);
            // $random .= chr(rand(ord('a'), ord('z')));
        }
        return $random;
    }

    public function generateUniqueId ($length = 8)
    {
    	$unique = '';
        do {
			$unique = $this->generateRandomKey($length);
		} while ($this->model->hasByUniqueId($unique));
		return $unique;
    }

    public function getAvatorPath ()
    {
        return $destinationPath = public_path() . '/' . Config::get('app.avatar_dir') . '/';
    }

}
