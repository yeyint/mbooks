<?php

class ReviewController extends\ BaseController {
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public
    function edit($id) {
        $book = Book::find($id)->toArray();
        $book['tag_id'] = unserialize($book['tag_id']);
        return View::make('book.review')->with(
            ['book' => $book, 'tags' => new Tag, ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public
    function update($id) {
        $tags = Input::get('tags');
        $exists = [];
        $new = [];
        $allTagIds = [];
        if ($tags != null) {
            foreach($tags as $tag) {
                $tag_result = Tag::where('id', $tag)->first();
                if ($tag_result) {
                    $exists[] = (string) $tag;
                } else {
                    $new[] = $tag;
                }
            }
            // Insert New Tag
            foreach($new as $tag) {
                $tag = Tag::create(['name' => $tag, ]);
                if ($tag) {
                    $allTagIds[] = (string) $tag->id;
                }
            }
            $allTagIds = array_merge($allTagIds, $exists);
        }
        $book = Book::find($id);
        $is_notes = Input::has('is_notes') ? true : false;
        $now = date('Y/m/d');
        // dd(Input::get('tags'));
        $success = Book::find($id)->update(['is_notes' => $is_notes, 'notes' => Input::get('notes'), 'review_at' => $now, 'tag_id' => serialize($allTagIds), ]);
        if ($success) {
            return $this->goToIndex();
        } else {
            return Redirect::back()->withInput()->withErrors($success->getErrors());
        }
    }
    private
    function goToIndex() {
        return Redirect::route(Config::get('app.backend_url').
            '.book.index');
    }
}