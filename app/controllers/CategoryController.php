<?php

class CategoryController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	
		$category = Category::orderBy('id', 'DESC')->get()->toArray();
		return View::make('category.index')->with(compact('category'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('category.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()


	{

		$name = Input::get('name_mm');
		$exists = Category::where('name_mm', $name)->first();
		if (!$exists) {
			$cat = Category::create([
				'name_mm' => $name,
				'custom_define_code' => Input::get('cd_code'),
				'name_en'            => Input::get('name_en'),
				'dd_code'            => Input::get('dd_code'),
				'dd_name'            => Input::get('dd_name'),
			]);
			if ($cat->save()) {
				return Redirect::route(Config::get('app.backend_url'). '.category.index');
			} else {

                return Redirect::back()->withInput()->withErrors($cat->getErrors());

			}
		} else {
			echo ('category is already exists');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$cat = Category::find($id)->toArray();
		return View::make('category.edit')->with(compact('cat'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$cat = Category::find($id)
						->update([
							'custom_define_code' => Input::get('cd_code'),
							'name_mm' => Input::get('name_mm'),
							'name_en' => Input::get('name_en'),
							'dd_code' => Input::get('dd_code'),
							'dd_name' => Input::get('dd_name'),
						]);
		return $this->goToIndex();
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		 Category::find($id)->delete();

		return $this->goToIndex();
	}

	private function goToIndex() {
		return Redirect::route(Config::get("app.backend_url"). '.category.index');
	}

}
