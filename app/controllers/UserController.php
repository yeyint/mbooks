<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = Sentry::findAllUsers();
		return View::make('user.index')->with([
			'users' => $users, 
			]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('user.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$user = Sentry::createUser(array(
			'first_name' => Input::get('first_name'),
			'last_name'  => Input::get('last_name'),
			'email'      => Input::get('email'),
			'password'   => Input::get('password'),
			'activated'  => true,
			'permissions' => array(
				'admin' => 1
			),
		));
		if( $user) {
			return Redirect::route(Config::get('app.backend_url').'.user.index');
		}else{

            return Redirect::back()->withInput();

        }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user=Sentry::findUserById($id);
        return View::make('user.edit',['user' => $user]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

        try
        {
            // Find the user using the user id
            $user = Sentry::findUserById($id);


            // Update the user details
            $user->first_name = Input::get('first_name');
            $user->last_name=Input::get('last_name');
            $user->email = Input::get('email');
            $user->password=Input::get('password');

            $user->save();
            // Update the user
            if ($user->save())
            {
                return Redirect::route(Config::get('app.backend_url').'.user.index');
            }
            else
            {
                // User information was not updated
            }
        }
        catch (Cartalyst\Sentry\Users\UserExistsException $e)
        {
            echo 'User with this login already exists.';
        }
        catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
        {
            echo 'User was not found.';
        }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$userId = $id;
		try
		{
		    $user = Sentry::findUserById($userId);
		    $user->delete();
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    throw new Exception('User was not found.', 1);
		}
		return $this->goToIndex();
	}


    private function goToIndex() {
        return Redirect::route(Config::get("app.backend_url"). '.user.index');
    }


}
