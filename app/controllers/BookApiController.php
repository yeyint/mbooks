<?php
class BookApiController extends \BaseController
{

    private $hashids;

    function __construct() {
        $this->hashids = new Hashids\Hashids(Config::get('app.salt_value'), 8, Config::get('random_string'));
        $this->categoryModel = new Category();
        $this->tagModel = new Tag();
        $this->authorModel = new Author();
        $this->publisherModel = new Publisher();
    }

    public function all($wrap = true) {
        $app = app();
        $books = $this->get(true);

        $categoryApiController = $app->make('CategoryApiController');
        $authorApiController = $app->make('AuthorApiController');
        $publisherApiController = $app->make('PublisherApiController');
        $editorpickApiController = $app->make('EditorpickApiController');

        $categories = $categoryApiController->callAction('get', array(true));
        $authors = $authorApiController->callAction('get', array(true));
        $publishers = $publisherApiController->callAction('get', array(true));
        $editorpicks = $editorpickApiController->callAction('get', array(true));
        return ($this->responseAll([$books, $categories, $authors, $publishers, $editorpicks], $wrap));
    }

    public function get($wrap = true) {

        /** inputs **/
        $id     = Input::get('book_id');
        $limit  = Input::get('limit');
        $by_author  = Input::get('by_author');
        $random = (boolean) Input::get('random');
        $skip   = (int) Input::get('skip');

        /** options/config **/
        $maxLimit = Config::get('api.book_limit.max');
        $defaultLimit = Config::get('api.book_limit.default');
        $limit = ($limit && $limit < $maxLimit) ? $limit : $defaultLimit;

        $mbooks = [];
        // Random List
        if ( $random ) {
            return $this->response($this->random($limit), $wrap);
        }
        // By category 
        elseif (Input::has('by_category')) {
            return $this->response($this->getByCategory(Input::get('by_category'), $limit, $skip), $wrap);
        }
        // By Author
        elseif (Input::has('by_author')) {
            return $this->response($this->getByAuthor(Input::get('by_author'), $limit, $skip), $wrap);
        }
        // All
        elseif ($id === null) {
            return $this->response($this->index($limit, $skip), $wrap);
        }
        // By ID
        return $this->response($this->show($id), $wrap);
    }

    public function response($books, $wrap) {
        if ($wrap) return [Config::get('api.book_wrapper') => $books];
        else return $books;
    }

    public function responseAll($mbooks, $wrap) {
        if ($wrap) return [Config::get('api.mbook_wrapper') => $mbooks];
        else return $mbooks;
    }

    // Get Random Books
    public function random($limit = 20) {
        $result = Book::orderByRaw("RAND()")->limit($limit)->orderBy('id', 'DESC')->get()->toArray();
        $book = [];
        foreach ($result as $key) {
            $book[] = $this->transformer($key);
        }
        return $book;
    }


    // Get all Books
    public function index($limit = 20, $skip = 0) {

        $result = Book::orderBy('id', 'DESC')->limit($limit)->skip($skip)->get()->toArray();

        $book = [];
        foreach ($result as $key) {
            $book[] = $this->transformer($key);
        }
        return $book;
    }

    /**
    * [foo description] 
    *
    * @return [type] [description]
    */
    public function getByAuthor($author_id, $limit = 20, $skip = 0)
    {
        $result = Book::orderBy('id', 'DESC')
                        ->where('author_id', $author_id)
                        ->limit($limit)
                        ->skip($skip)
                        ->get()
                        ->toArray();

        $book = [];
        foreach ($result as $key) {
            $book[] = $this->transformer($key);
        }
        return $book;

    }

    /**
    * [foo description] 
    *
    * @return [type] [description]
    */
    public function getByCategory($category_id, $limit = 20, $skip = 0)
    {
        $result = Book::orderBy('id', 'DESC')
                        ->where("category_id", "LIKE", "%:\"" . $category_id . "\";%")
                        ->limit($limit)
                        ->skip($skip)
                        ->get()
                        ->toArray();

        $book = [];
        foreach ($result as $key) {
            $book[] = $this->transformer($key);
        }
        return $book;

    }

    // Get book by id
    public function show($id) {
        $id = $this->hashids->decode($id);

        $result = Book::find($id);
        $book = [];
        if (!$result) {
            return $book;
        }
        foreach ($result as $key) {
            $book[] = $this->transformer($key);
        }
        return $book;
    }

    public function transformer($book) {
        $api = Config::get('api.book');
        $transformed = [];
        foreach ($api as $key => $value) {

            //$transformed[$value]=$this->authorModel->getName($book['author_id']);

            if ($key == 'cover_pic') {

                $transformed[$value] = (isset($book[$key]) && $book[$key]) ? $this->getAvatarUrl() . $book[$key] : '';
            }
            elseif ($key == 'category_id') {

                $cat_ids = $this->categoryModel->getNames($book['category_id'], 1);
                $transformed[$value] = $cat_ids;
            }
            elseif ($key == 'tag_id') {

                $tags_ids = $this->tagModel->getNames($book['tag_id'], 1);
                $transformed[$value] = $tags_ids;
            }
            elseif ($key == 'author_id') {

                $author_id = $this->authorModel->getName($book['author_id']);
                $transformed[$value] = array($book['author_id'] => $author_id);
            }
            elseif ($key == 'publisher_id') {

                $publisher_id = $this->publisherModel->getName($book['publisher_id']);
                $transformed[$value] = array($book['publisher_id'] => $publisher_id);
            }
            elseif ($key == 'related_books') {
                $transformed[$value] = $this->getRelatedBooks($book['category_id'], $book['tag_id']);
            }
            elseif ($key == 'authors_books') {
                $transformed[$value] = $this->getAuthorsBooks($book['author_id']);
            }
            else {
                $transformed[$value] = (isset($book[$key]) && $book[$key]) ? $book[$key] : '';
            }
        }
        return $transformed;
    }

    public function generateUniqueId($length) {
        $random = '';
        for ($i = 0; $i < $length; $i++) {
            $random.= chr(rand(ord('a'), ord('z')));
        }
        return $random;
    }

    public static function getAvatarUrl() {
        return URL::to('/') . '/' . Config::get('app.avatar_dir') . '/';
    }

    public function getAuthorsBooks( $author_id)
    {
        $authorsBooks = [];
        $books = Book::where('author_id', $author_id)
                        ->whereNotIn('unique_id', [Input::get('book_id')])
                        ->orderBy(DB::raw('RAND()'))
                        ->limit(10)
                        ->get();
        if ($books) {
            foreach ($books as $key => $value) {
                $authorsBooks[] = [
                    'key'       => $value->unique_id,
                    'cover_pic' => $this->getAvatarUrl() . $value->cover_pic,
                ];
            }
        }
        return ($authorsBooks);
    }

    public function getRelatedBooks($category_id, $tag_id) {

        // Find books by tag
        if ($tag_id) {
            $tags = unserialize( $tag_id );
        } else {
            return [];
        }

        $relatedWithCategory = [];
        foreach ($tags as $key => $value) {
            $_relatedWithCategory = Book::
                                        // where("category_id", "LIKE", "%:\"" . $value . "\";%")
                                        orWhere("tag_id", "LIKE", "%:\"" . $value . "\";%")->orderBy(DB::raw('RAND()'))
                                        ->whereNotIn('unique_id', [Input::get('book_id')])
                                        ->orderBy(DB::raw('RAND()'))->limit(10)->get();
            if ($_relatedWithCategory) {
                // $_relatedWithCategory = $_relatedWithCategory->toArray();
                foreach ($_relatedWithCategory as $mkey => $mval) {
                    $unique_id = $mval->unique_id;
                    $relatedWithCategory[$unique_id] = ['key' => $unique_id, 'cover_pic' => $this->getAvatarUrl() . $mval->cover_pic, ];
                }
            }
        }
        return array_values($relatedWithCategory);
        // Find books by tag
        // $tags = unserialize($tag_id);

        // $relatedWithTag = [];
        // foreach ($tags as $key => $value) {
        //     $_relatedWithTag = Book::where("tag_id", "LIKE", "%:\"" . $value . "\";%")->orderBy(DB::raw('RAND()'))->limit(5)->get();
        //     if ($_relatedWithTag) {
        //         // $_relatedWithTag = $_relatedWithTag->toArray();
        //         foreach ($_relatedWithTag as $mkey => $mval) {
        //             $unique_id = $mval->unique_id;
        //             $relatedWithTag[$unique_id] = ['key' => $unique_id, 'cover_pic' => $mval->cover_pic, ];
        //         }
        //     }
        // }
        // // Merge array
    }
}
