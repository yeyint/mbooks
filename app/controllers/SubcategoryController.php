<?php

class SubcategoryController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$subcategory = Subcategory::orderBy('id', 'DESC')->get()->toArray();
		return View::make('subcategory.index')->with(

           [
             'subcategory' => $subcategory,
             'category'    => new Category,
            ]

			);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{   
		$category=new Category;
		return View::make('subcategory.create')->with(compact('category'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()


	{

		$name = Input::get('name_mm');
		$exists = Subcategory::where('name_mm', $name)->first();
		if (!$exists) {
			$cat = Subcategory::create([
				'name_mm' => $name,
				'name_en' => Input::get('name_en'),
				'main_category_id' => Input::get('main_category_id'),
				'dd_code' => Input::get('dd_code'),
				'dd_name' => Input::get('dd_name'),
			]);
			if ($cat->save()) {
				return Redirect::route(Config::get('app.backend_url'). '.subcategory.index');
			} else {

                return Redirect::back()->withInput()->withErrors($cat->getErrors());

			}
		} else {
			echo ('subcategory is already exists');
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{   
		$subcategory = Subcategory::find($id)->toArray();
		return View::make('subcategory.edit')->with(
            array(
                  'subcategory' => $subcategory,
                  'category' => new Category,
            

            	)
			);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$cat = Subcategory::find($id)
						->update([
							'name_mm'       => Input::get('name_mm'),
							'name_en'       => Input::get('name_en'),
							'main_category_id' => Input::get('main_category_id'),
							'dd_code'       => Input::get('dd_code'),
							'dd_name'       => Input::get('dd_name'),
						]);
		return $this->goToIndex();
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		 Subcategory::find($id)->delete();

		return $this->goToIndex();
	}

	private function goToIndex() {
		return Redirect::route(Config::get("app.backend_url"). '.subcategory.index');
	}

}