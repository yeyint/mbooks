<?php

class CategoryApiController extends \BaseController {

	public function get ($wrap = true)
    {
        return $this->response($this->index(), $wrap);
        // $id = Input::get('id');
        // if ($id === null) {
        // }
        // return $this->show($id);
    }


    public function response($category, $wrap) {
        if ($wrap)
            return [Config::get('api.category_wrapper') => $category];
        else 
            return $category;
    }

    public function index()
    {
        $result = Category::all()->toArray();
        $categories = [];
        foreach ($result as $key) {
            $categories[] = $this->transformer($key);
        }
        return $categories;
    }

    public function transformer($category) {
        $api = Config::get('api.category');
        $transformed = [];
        foreach ($api as $key => $value) {
            $transformed[$value] = (isset($category[$key]) && $category[$key] ) ? $category[$key]: '';
        }
        return $transformed;
    }

}
