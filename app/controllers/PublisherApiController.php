<?php

class PublisherApiController extends \BaseController {

	public function get ($wrap = true)
    {
        return $this->response($this->index(), $wrap);
        // $id = Input::get('id');
        // if ($id === null) {
        // }
        // return $this->show($id);
    }


    public function response($publisher, $wrap) {
        if ($wrap)
            return [Config::get('api.publisher_wrapper') => $publisher];
        else 
            return $publisher;
    }

    public function index()
    {
        $result = Publisher::all()->toArray();
        $categories = [];
        foreach ($result as $key) {
            $categories[] = $this->transformer($key);
        }
        return $categories;
    }

    public function transformer($publisher) {
        $api = Config::get('api.publisher');
        $transformed = [];
        foreach ($api as $key => $value) {
            $transformed[$value] = (isset($publisher[$key]) && $publisher[$key] ) ? $publisher[$key]: '';
        }
        return $transformed;
    }

}
