<?php

class FrontendController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function home()
    {
        $books = (json_decode(file_get_contents(public_path("book.json"))));
        $books = ($books->books);
        // dd($books);
        return View::make('frontend.pages.home')->with([
            'books' => $books
            ]);
	}


}
