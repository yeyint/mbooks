<?php

class AuthorController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $authors = Author::orderBy('id', 'DESC')->get()->toArray();
        return View::make('author.index')->with(compact('authors'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('author.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $destinationPath = $this->getAvatorPath();
        $filename        = null;
        $share_pic       = null;

        if (Input::hasFile('avatar')) {
            $file            = Input::file('avatar');
            $filename        = str_random(4) . '_'. date('d_m_Y') .'.jpg';
            $uploadSuccess   = $file->move($destinationPath, $filename);
            $path = $destinationPath . $filename;
            $img = Image::make($path)->resize(100, 100);
            $img->save($path);
            // return $img->response('jpg');
        }

        if (Input::hasFile('share_pic')) {
            $file            = Input::file('share_pic');
            $share_pic       = str_random(4) . '_'. date('d_m_Y') .'.jpg';
            $uploadSuccess   = $file->move($destinationPath, $share_pic);
            $path = $destinationPath . $share_pic;
            $img = Image::make($path);
            // ->resize(600, 480);
            $img->save($path);
            // return $img->response('jpg');
        }

        $success = Author::create([
            'name' => Input::get('name'),
            'profile_pic' => $filename,
            'share_pic' => $share_pic,
            'brief' => Input::get('brief'),
        ]);

        if ($success ->save()) {
            return $this->goToIndex();
        } else {
            return Redirect::back()->withInput()->withErrors($success->getErrors());
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $author = Author::find($id)->toArray();
        return View::make('author.edit')->with(compact('author'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update( $id )
    {
        $destinationPath = $this->getAvatorPath();
        $filename = null;
        $author = Author::find($id);
        if (!$author) {
            throw new Exception("Author Not Found", 1);
        }

/*        if(
            isset($author->profile_pic) 
            && $author->profile_pic 
            && is_file($destinationPath.$author->profile_pic)) {
            unlink( $destinationPath.$author->profile_pic );
        }*/

        if (Input::hasFile('avatar')) {
            $file            = Input::file('avatar');
            $filename        = str_random(4) . '_'. date('d_m_Y') .'.jpg';
            $uploadSuccess   = $file->move($destinationPath, $filename);
            $path = $destinationPath . $filename;
            $img = Image::make($path)->resize(100, 100);
            $img->save($path);
            // return $img->response('jpg');
        }elseif (!$author->profile_pic) {
            
           $filename=null;

        }else{
 
           $filename=$author->profile_pic;
           $path = $destinationPath . $filename;
           $img = Image::make($path)->resize(100, 100);
           $img->save($path);

        }

        $success = Author::find($id)
                            ->update([
                                'name' => Input::get('name'),
                                'brief' => Input::get('brief'),
                                'profile_pic' => $filename,
                             ]);
        if ($success ) {
            return $this->goToIndex();
        } else {
            return Redirect::back()->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $destinationPath = $this->getAvatorPath();
        $author = Author::find($id);
        if(
            isset($author->profile_pic) 
            && $author->profile_pic 
            && is_file($destinationPath.$author->profile_pic)) {
            unlink( $destinationPath.$author->profile_pic );
        }
        $success = $author->delete();
        if ($success) {
            return $this->goToIndex();
        }
        return ['success' => 0];
    }

    private function goToIndex() {
        return Redirect::route(Config::get('app.backend_url').'.author.index');
    }

    public function getAvatorPath (  )
    {
        return $destinationPath = public_path() . '/' . Config::get('app.avatar_dir') . '/';
    }
}
