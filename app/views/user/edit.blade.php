@extends('layouts.master')
@section('styles')

@stop


@section('content')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-users fa-fw"></i>Edit User
                    <span> <a class="page-header" href="{{route(Config::get('app.backend_url').'.user.index')}}">Back</a></span>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    {{Form::open([
                        'url' => route(Config::get('app.backend_url').'.user.update',$user['id']),
                        'role' => 'form',
                        'class' => 'col-lg-12 margin-bottom-40',
                        'method' => 'PUT',
                    ])}}
                    <div class="form-group">

                        {{
                            Form::text('first_name',$user['first_name'],  [
                                'class'     => 'form-control margin-bottom-20',
                                'autofocus' => true,
                                'placeholder' => 'First Name',
                            ])
                        }}

                    </div><div class="form-group">
                        {{
                            Form::text('last_name',$user['last_name'],  [
                                'class'=> 'form-control margin-bottom-20',
                                'placeholder' => 'Last Name',
                            ])
                        }}
                    </div> <div class="form-group">
                        {{
                            Form::text('email',$user['email'],  [
                                'class'    => 'form-control margin-bottom-20',
                                'placeholder' => 'E-Mail',
                            ])
                        }}
                    </div>
                        
                        <!-- Password Form Input-->
                        
                        <div class="form-group">

                            {{Form::password('password',['class' => 'form-control margin-bottom-20','placeholder' => 'Password'])}}
                        </div>

                    <div class="form-group">
                        {{--submit--}}
                        {{Form::submit('Update', [
                            'class' => 'btn btn-success',
                        ])}}</div>

                    {{Form::close()}}


@stop


