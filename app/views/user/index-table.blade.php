<div class="dataTable_wrapper">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
        <tr>
            <td class="col-xs-1">Username</td>
            <td class="col-xs-2">Email</td>
            <td class="col-xs-2">Actions</td>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr class="gradeX">
                <td>{{ $user->first_name }}{{ $user->last_name }}</td>
                <td>{{ $user->email }}</td>
                <td data-id="{{$user->id}}" class="td-actions">

                    <a class='btn btn-info btn-xs'
                       href="{{route(Config::get('app.backend_url').'.user.edit', $user['id'])}}" title="Edit"><span class="glyphicon glyphicon-edit"></span> Edit</a>


                    @include('partials.modal',['data'=>$user,'name' => 'user'])


                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@section('script')
<script>
$('.btn-delete').on('click', function() {
    var id = $(this).data("id");
    var sure = confirm("Are you sure?");
    if (sure) {
        $.ajax({
            url: vars.current_url + "/" + id,
            method: 'DELETE',
            type: 'JSON',
            success: function(data) {
                console.log(data);
                if ( data.success )
                    window.location.reload(false);
                else
                    alert('error');
            }
        });
    }
});
</script>
@stop
