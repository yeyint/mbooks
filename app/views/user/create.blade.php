@extends('layouts.master')
@section('styles')

@stop


@section('content')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-users fa-fw"></i>Add New User
                    <span> <a class="page-header"
                              href="{{route(Config::get('app.backend_url').'.user.index')}}">Back</a></span></h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    {{Form::open([
                        'url' => route(Config::get('app.backend_url').'.user.store'),
                        'role' => 'form',
                        'class' => 'col-lg-12 margin-bottom-40',
                        'method' => 'POST',
                    ])}}
                    <div class="form-group">

                        {{
                            Form::text('first_name',null,  [
                                'class'     => 'form-control margin-bottom-20',
                                'autofocus' => true,
                                'placeholder' => 'First Name',
                            ])
                        }}

                    </div>
                    <div class="form-group">
                        {{
                            Form::text('last_name',null,  [
                                'class'=> 'form-control margin-bottom-20',
                                'placeholder' => 'Last Name',
                            ])
                        }}
                    </div>
                    <div class="form-group">
                        {{
                            Form::text('email',null,  [
                                'class'    => 'form-control margin-bottom-20',
                                'placeholder' => 'E-Mail',
                            ])
                        }}
                    </div>
                    <div class="form-group">
                        {{
                            Form::password('password', [
                                'class'=> 'form-control margin-bottom-20',
                                'placeholder' => 'Password',
                            ])
                        }}
                    </div>
                    <div class="form-group">
                        {{--submit--}}
                        {{Form::submit('Add', [
                            'class' => 'btn btn-success',
                        ])}}</div>

                    {{Form::close()}}
                </div>

            </div>
        </div>
    </div>

@stop


