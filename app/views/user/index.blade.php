@extends('layouts.master')

@section('content')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"> <i class="fa fa-users fa-fw"></i> Users
                    <a class="btn btn-success pull-right"
                       href="{{route(Config::get('app.backend_url').'.user.create')}}"
                            ><i class="fa fa-plus"></i> Add User</a>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                {{-- Table --}}
                @include('user.index-table', array('some' => 'data'))
                {{-- end of table --}}
            </div>
        </div>
    </div>
@stop
