@extends('layouts.master')

@section('content')
<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-list-alt fa-fw"></i>Editor Pick
                    <span> <a class="page-header" href="{{route(Config::get('app.backend_url').'.editorpick.index')}}">Back</a></span>
                </h1>


            </div>

            <!-- /.col-lg-12 -->
        </div>
        <div class="row">


                    {{Form::open([
                        'url' => route(Config::get('app.backend_url').'.editorpick.store'),
                        'role' => 'form',
                        'class' => 'col-lg-12',
                        'method' => 'POST',
                    ])}}

                  <div class="form-group">
                      {{ Form::select('book_id',$book->getForSelect(), null, ['class' => 'form-control']) }}
                  </div>


                  <div class="form-group">
                      {{ Form::submit('Add', ['class' => 'btn btn-primary ']) }}
                  </div>

                    {{Form::close()}}

        </div>
    </div>
    
@stop
