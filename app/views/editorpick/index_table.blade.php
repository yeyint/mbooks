
<div class="table-responsive">
    <table class="table table-bordered table-striped" id="dataTables-example">
        <thead>
        <tr>
            <!-- <td class="col-xs-1">ID</td> -->

            <td class="">ID</td>
            <td class="">Book Name</td>
            <td class="">Author</td>
            <td class="">Tags</td>
        </tr>
        </thead>
        <tbody>
        @foreach($picks->toArray() as $pick)
            <tr class="gradeX">
                <td>{{$pick['id']}}</td>
                <td>{{Str::limit($pick['book_title'], 100)}}</td>
                <td>{{$author->getName($pick['author_name'])}}</td>
                
                <td class="center">

                    <a class='btn btn-info btn-xs' href="{{route(Config::get('app.backend_url').'.editorpick.edit', $pick['id'])}}" title="Edit"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                    {{--<a
                            data-id="{{$pick['id']}}"
                            href="{{route(Config::get('app.backend_url').'.pick.destroy', $pick['id'])}}"
                            class="btn btn-danger" title="Delete">Delete</a>--}}
                    @include('partials.modal', ['data' => $pick, 'name' => 'editorpick'])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

