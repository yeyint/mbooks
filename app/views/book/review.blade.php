@extends('layouts.master')
@section('content')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-book fa-fw"></i>Review Book
                    <span> <a class="page-header" href="{{route(Config::get('app.backend_url').'.book.index')}}">Back</a></span>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    {{Form::open([
                        'url' => route(Config::get('app.backend_url').'.review.update', $book['id']),
                        'role' => 'form',
                        'files' => true,
                        'class' => 'col-sm-12 margin-bottom-40',
                        'method' => 'PUT',
                    ])}}
                   <div class="form-group">
                            {{ Form::label('is_notes', 'Notes:') }}
                            {{ Form::checkbox('is_notes',null,$book['is_notes'],['class' => 'is_notes']) }}
                    </div>    
   

                    <div class="form-group" id="notes">
                    {{Form::textarea('notes',$book['notes'],  [
                        'class'=> 'form-control ',
                        'placeholder' => 'Notes',
                    ])}}
                        {{ $errors->first('title', '<div class="text-danger">:message</div>') }}
                   </div>

                {{--     <div class="form-group">
                        <label>Tags</label>
                    <select name="tags[]" multiple class="book-tags form-control">
                     @if($book_tags)
                     @foreach ($book_tags as $value)
                      <option selected value="{{$value['id']}}" >             
                     {{$value['name']}}
                      </option>
                      @endforeach         
                     @else
                     @foreach ($tags->getForSelect as $value)
                      <option  value="{{$value['id']}}" >             
                     {{$value['name']}}
                      </option>
                     @endforeach
                     @endif           
                    </select>
                    </div> --}}
                     <div class="form-group ">
                    {{-- Cat --}}
                    <label>Tags</label>
                    {{
                        Form::select('tags',
                            $tags->getForSelect()
                            ,$book['tag_id'],[
                            'class'   => 'book-tags form-control margin-bottom-10  ',
                            'multiple' => 1,
                            'name' => 'tags[]'
                            ])
                        }}
                        </div>

                    <div class="form-group">

                    {{Form::submit('Update', [
                        'class' => 'btn btn-success',
                    ])}}

                    </div>

                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
@stop







