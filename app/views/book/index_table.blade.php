
<div class="table-responsive">
    <table class="table table-bordered table-striped" id="dataTables-example">
        <thead>
        <tr>
            <!-- <td class="col-xs-1">ID</td> -->
            <td class="col-sm-2">Title</td>
            <td class="col-sm-2">Book Cover</td>
            
            <td class="col-sm-2">Category</td>
            <td class="">Author</td>
            <td class="">Tags</td>
        </tr>
        </thead>
        <tbody>
        @foreach($books->toArray()['data'] as $book)
            <tr class="gradeX">
                <!-- <td>{{$book['id']}}</td> -->
                <td>{{Str::limit($book['title'], 100)}}</td>
                <td>
                    <div class="row">
                        <div class="col-xs-12">
                            <?php $img =  URL::to('/'). '/'.Config::get('app.avatar_dir').'/' . $book['cover_pic'];  ?>
                            @if (is_file( public_path().'/'. Config::get('app.avatar_dir') . '/' . $book['cover_pic']))
                                <div class="profile_pic">
                                    <img src="{{ $img }}" width="80" height="120">
                                </div>
                            @else
                                -
                            @endif
                        </div>
                    </div>
                    Link: {{$book['cover_pic']}}
                </td>
                {{-- <td>{{Str::limit($book['subjects'], 120)}}</td> --}}

                <td>
                    {{$category->getNames($book['category_id'])}}
                </td>
                <td>{{$author->getName($book['author_id'])}}</td>
                <td class="center">

                    <a class='btn btn-primary btn-xs' href="{{route(Config::get('app.backend_url').'.review.edit', $book['id'])}}" title="Review"><span class="glyphicon glyphicon-search"></span> Review</a>

                    <a class='btn btn-info btn-xs' href="{{route(Config::get('app.backend_url').'.book.edit', $book['id'])}}" title="Edit"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                    {{--<a
                            data-id="{{$book['id']}}"
                            href="{{route(Config::get('app.backend_url').'.book.destroy', $book['id'])}}"
                            class="btn btn-danger" title="Delete">Delete</a>--}}
                    {{-- for delete popup box --}}
                    @include('partials.modal', ['data' => $book, 'name' => 'book'])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
{{ $books->links();}}
