@extends('layouts.master')
@section('content')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-book fa-fw"></i>Edit Book
                    <span> <a class="page-header" href="{{route(Config::get('app.backend_url').'.book.index')}}">Back</a></span>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    {{Form::open([
                        'url' => route(Config::get('app.backend_url').'.book.update', $book['id']),
                        'role' => 'form',
                        'files' => true,
                        'class' => 'col-sm-12 margin-bottom-40',
                        'method' => 'PUT',
                    ])}}
                   <div class="form-group">
                    {{--3--}}

                    <label>Full Title</label>
                    {{Form::text('title',$book['title'],  [
                        'class'=> 'form-control ',
                         'id' => "title_editor",
                         
                    ])}}
                        {{ $errors->first('title', '<div class="text-danger">:message</div>') }}
                        </div>

                    <div class="form-group ">
                    {{-- Cat --}}
                    <label>Category</label>
                    {{
                        Form::select('category',
                            $category->getForSelect()
                            ,$book['category_id'],[
                            'class'   => 'book-category form-control margin-bottom-10  ',
                            'multiple' => 1,
                            'name' => 'categories[]'
                            ])
                        }}
                        </div>
                    <div class="form-group">
                    {{-- Author --}}
                    <label>Author</label>
                    {{
                        Form::select('author',
                            $author->getForSelect()
                            ,$book['author_id'],[
                            'class'   => 'form-control margin-bottom-10',
                            ])
                        }}

                    </div>
                    <div class="form-group">
                    {{--3--}}

                    <label>Publisher</label>
                    {{
                        Form::select('publisher',
                            $publisher->getForSelect()
                            ,$book['publisher_id'],[
                            'class'   => 'form-control margin-bottom-10',
                            ])
                        }}
                        {{ $errors->first('title', '<div class="text-danger">:message</div>') }}
                        </div>    
                    
                    <div class="form-group">
                    <label>Book Cover</label>
                    {{Form::file('cover')}}
                    </div>

                    <div class="form-group">
                    <label>Share Picture</label>
                    {{Form::file('share_pic')}}

                    </div>
        
                    <div class="form-group">
                    {{--3--}}
                    <label>Subjects</label>
                    {{Form::textarea('subjects',$book['subjects'],  [
                        'class'=> 'form-control margin-bottom-10 summery',
                         'id' => "subjects_editor",
                        'placeholder' => 'Subjects',
                    ])}}
                        </div>

                    {{--3--}}

 
                    <div class="form-group">

                

                            {{ Form::label('is_notes', 'Notes:') }}
                            {{ Form::checkbox('is_notes',null,$book['is_notes'],['class' => 'is_notes']) }}
                    </div>    

                    {{--3--}}
                    <div class="form-group" id="notes">                
                    {{Form::textarea('notes',$book['notes'],  [
                        'class'=> 'form-control ',
                        'placeholder' => 'Notes',
                    ])}}
                        {{ $errors->first('title', '<div class="text-danger">:message</div>') }}
                   </div>
                   <div class="form-group">
                    {{--3--}}

                    <label>Physical Description</label>
                    {{Form::text('physical_description',$book['physical_description'],  [
                        'class'=> 'form-control ',
                         'id' => "physical_editor",
                        'placeholder' => 'Physical Description',
                    ])}}
                        {{ $errors->first('isbn', '<div class="text-danger">:message</div>') }}
                        </div>
                    <div class="form-group">

                    <div class="form-group">

                    {{--3--}}

                    <label>ISBN</label>
                    {{Form::text('isbn',$book['isbn'],  [
                        'class'=> 'form-control ',
                         'id' => "isbn_editor",
                        'placeholder' => 'Title',
                    ])}}
                        {{ $errors->first('isbn', '<div class="text-danger">:message</div>') }}
                        </div>
                    <div class="form-group">
                        {{ Form::label('pages_no', 'Pages_no:') }}
                        {{ Form::text('pages_no', $book['pages_no'], ['class' => 'form-control']) }}
                    </div>

                    <div class="form-group">
                    {{--submit--}}
                    {{Form::submit('Update', [
                        'class' => 'btn btn-success',
                    ])}}
                    </div>

                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
@stop

