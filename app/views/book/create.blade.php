@extends('layouts.master')
@section('styles')
    <style>
        #cke_editor2,
        #cke_editor1 {
            margin-bottom: 20px;
        }
    </style>
@stop


@section('content')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-book fa-fw"></i>Add New Book
                    <span> <a class="page-header" href="{{route(Config::get('app.backend_url').'.book.index')}}">Back</a></span>
                </h1>

            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="row">


                    {{Form::open([
                        'url' => route(Config::get('app.backend_url').'.book.store'),
                        'role' => 'form',
                        'class' => 'col-lg-12 margin-bottom-40',
                        'files' => true,
                        'method' => 'POST',
                    ])}}
                    <div class="form-group">
                    {{--3--}}

                    <label>Full Title</label>
                    {{Form::text('title',null,  [
                        'class'=> 'form-control ',
                         'id' => "title_editor",
                        'placeholder' => 'Title',
                    ])}}
                        {{ $errors->first('title', '<div class="text-danger">:message</div>') }}
                        </div>

                    <div class="form-group ">
                    {{-- Cat --}}
                    <label>Category</label>
                    {{
                        Form::select('category',
                            $category->getForSelect()
                            ,null,[
                            'class'   => 'book-category form-control margin-bottom-10  ',
                            'multiple' => 1,
                            'name' => 'categories[]'
                            ])
                        }}
                        </div>
                    <div class="form-group">
                    {{-- Author --}}
                    <label>Author</label>
                    {{
                        Form::select('author',
                            $author->getForSelect()
                            ,null,[
                            'class'   => 'form-control margin-bottom-10',
                            ])
                        }}

                        </div>
                    <div class="form-group">
                    {{--3--}}

                    <label>Publisher</label>
                    {{
                        Form::select('publisher',
                            $publisher->getForSelect()
                            ,null,[
                            'class'   => 'form-control margin-bottom-10',
                            ])
                        }}
                        {{ $errors->first('title', '<div class="text-danger">:message</div>') }}
                        </div>    
                    
                    <div class="form-group">
                    <label>Book Cover</label>
                    {{Form::file('cover')}}

                    </div>
                    <div class="form-group">
                    <label>Share Picture</label>
                    {{Form::file('share_pic')}}

                    </div>
                    <div class="form-group">
                    {{--3--}}
                    <label>Subjects</label>
                    {{Form::textarea('subjects',null,  [
                        'class'=> 'form-control margin-bottom-10 summery ',
                         'id' => "subjects_editor",
                        'placeholder' => 'Subjects',
                    ])}}
                        </div>

                    <div class="form-group">
                            {{ Form::label('is_notes', 'Notes:') }}
                            {{ Form::checkbox('is_notes',null,null,['class' => 'is_notes']) }}
                    </div>    

                    {{--3--}}
                    <div class="form-group" id="notes">                
                    {{Form::textarea('notes',null,  [
                        'class'=> 'form-control ',
                        'placeholder' => 'Notes',
                    ])}}
                        {{ $errors->first('title', '<div class="text-danger">:message</div>') }}
                   </div>
                   

                    {{--3--}}
                    <div class="form-group">

                    <label>Physical Description</label>
                    {{Form::text('physical_description',null,  [
                        'class'=> 'form-control ',
                         'id' => "physical_editor",
                        'placeholder' => 'Physical Description',
                    ])}}
                        {{ $errors->first('isbn', '<div class="text-danger">:message</div>') }}
                    </div>
                    

                    {{--3--}}
                    <div class="form-group">

                    <label>ISBN</label>
                    {{Form::text('isbn',null,  [
                        'class'=> 'form-control ',
                         'id' => "isbn_editor",
                        'placeholder' => 'ISBN',
                    ])}}
                        {{ $errors->first('isbn', '<div class="text-danger">:message</div>') }}
                        </div>

                     <div class="form-group">
                            {{ Form::label('pages_no', 'Pages_no:') }}
                            {{ Form::text('pages_no', null, ['class' => 'form-control']) }}
                        </div>   

                    {{--submit--}}
                    {{Form::submit('Add', [
                        'class' => 'btn btn-success lorem',
                    ])}}
                    </div>

                    {{Form::close()}}

                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
<script type="text/javascript">
    
$(document).ready(function() {
    $('#notes').hide();
    $('.is_notes').next().on('click', function() {
        $('#notes').toggle();
    });
      
    var value=$('.multiple').val();
    console.log(value);

});

</script>

@stop


