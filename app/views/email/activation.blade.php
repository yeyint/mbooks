<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Account Activation</title>
	<style type="text/css">
		body, html {
		    margin: 0 auto;
		    padding: 0;
		    height: 100%;
		    box-shadow: 2p 2px 2px #e3e3e3;
		}
		iframe {
			margin-top: 20px;
			margin-left: 10%;
			margin-right: 10%;
			padding: 10px;
			width: 80%;
			height: 80%;
		}
		.link {
			margin-top: 20px;
			margin-left: 10%;
			margin-right: 10%;
		}
	</style>
</head>
<body>
<div class="link">
	<h3><a target="_blank" href="{{$activationLink}}">Click to activate your account</a></h3>
</div>
</body>
</html>