<div class="table-responsive">
    <table class="table table-bordered table-striped" id="dataTables-example">
        <thead>
        <tr>
            <td class="">ID</td>
            <td class="">Publisher Name</td>
            <td class="">Publisher Address</td>
            <td class="">Phone</td>
            <td class="">Actions</td>
        </tr>
        </thead>
        <tbody>
        @foreach($publishers as $publisher)
            <tr class="gradeX">
                <td>{{$publisher['id']}}</td>
                <td>{{$publisher['name']}}</td>
                <td>{{$publisher['address']}}</td>
                <td>{{$publisher['phone']}}</td>
                
                <td class="center">

                    <a class='btn btn-info btn-xs' href="{{route(Config::get('app.backend_url').'.publisher.edit', $publisher['id'])}}" title="Edit"><span class="glyphicon glyphicon-edit"></span> Edit</a>
{{--                    <a
                       data-id="{{$publisher['id']}}"
                       href="{{route(Config::get('app.backend_url').'.publisher.destroy', $publisher['id'])}}"
                       class="fa btn-delete fa-trash action-btn" title="Delete"></a>--}}
                    @include('partials.modal', ['data' => $publisher, 'name' => 'publisher'])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
