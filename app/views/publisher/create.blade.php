@extends('layouts.master')

@section('content')


    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-list-alt fa-fw"></i>New Publisher
                    <span> <a class="page-header" href="{{route(Config::get('app.backend_url').'.publisher.index')}}">Back</a></span>
                </h1>


            </div>

            <!-- /.col-lg-12 -->
        </div>
        <div class="row">


                    {{Form::open([
                        'url' => route(Config::get('app.backend_url').'.publisher.store'),
                        'role' => 'form',
                        'class' => 'col-lg-12',
                        'method' => 'POST',
                    ])}}
                      <div class="form-group">
                        {{ Form::label('name', 'Name:') }}
                        {{ Form::text('name', null, ['class' => 'form-control']) }}
                      </div>

                      <div class="form-group">
                        {{ Form::label('address', 'Address:') }}
                        {{ Form::text('address', null, ['class' => 'form-control']) }}
                      </div>

                      <div class="form-group">
                        {{ Form::label('phone_no', 'Phone_no:') }}
                        {{ Form::text('phone_no', null, ['class' => 'form-control']) }}
                      </div>

  
                       <div class="form-group">
                        {{Form::submit('Add', [
                            'class' => 'btn btn-success  ',
                        ])}}
                         </div>

                    {{Form::close()}}

        </div>
    </div>
@stop