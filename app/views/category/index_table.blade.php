<div class="table-responsive">
    <table class="table table-bordered table-striped" id="dataTables-example">
        <thead>
        <tr>
            <td class="">ID</td>
            <td class="">Custom Define Code</td>
            <td class="">Name Myanmar</td>
            <td class="">Name English</td>
            <td class="">Dewey Decimal Code</td>
            <td class="">Dewey Decimal Name</td>
            <td class="">Actions</td>
        </tr>
        </thead>
        <tbody>
        @foreach($category as $cat)
            <tr class="gradeX">
                <td>{{$cat['id']}}</td>
                <td>{{$cat['custom_define_code']}}</td>
                <td>{{$cat['name_mm']}}</td>
                <td>{{$cat['name_en']}}</td>
                <td>{{$cat['dd_code']}}</td>
                <td>{{$cat['dd_name']}}</td>
                <td class="center">

                    <a class='btn btn-info btn-xs' href="{{route(Config::get('app.backend_url').'.category.edit', $cat['id'])}}" title="Edit"><span class="glyphicon glyphicon-edit"></span> Edit</a>
{{--                    <a
                       data-id="{{$cat['id']}}"
                       href="{{route(Config::get('app.backend_url').'.category.destroy', $cat['id'])}}"
                       class="fa btn-delete fa-trash action-btn" title="Delete"></a>--}}
                    @include('partials.modal', ['data' => $cat, 'name' => 'category'])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@section('scripts')
<script>
    (function($) {
        $('.btn-delete').click(function(e) {
            var confirms = confirm('Are you really want to "DELETE"');
            if (!confirms) {
                return;
            }
            e.preventDefault();
            var id = $(this).data('id'),
                href = $(this).attr('href');
            
            $.ajax({
                url: href,
                method: 'DELETE',
                data: {},
                type: 'JSON'
            }).success(function(data) {
                if(data.success) {
                    window.location = window.location;
                }
            });
        });
    })(jQuery)
</script>
@stop