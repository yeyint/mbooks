@extends('layouts.master')

@section('content')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"> <i class="fa fa-cube fa-fw"></i> Edit Category
                    <span> <a class="page-header" href="{{route(Config::get('app.backend_url').'.category.index')}}">Back</a></span>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">

                    {{Form::open([
                        'url' => route(Config::get('app.backend_url').'.category.update', $cat['id']),
                        'role' => 'form',
                        'class' => 'col-lg-12',
                        'method' => 'PUT',
                    ])}}
                    <div class="form-group">
                        {{ Form::label('cd_code', 'Custom_Define_Code:') }}
                        {{ Form::text('cd_code', $cat['custom_define_code'], ['class' => 'form-control']) }}
                    </div>

                    <div class="form-group">
                    {{ Form::label('name_mm', 'Name(Myanmar):') }}
                    {{Form::text('name_mm', $cat['name_mm'],  [
                        'class'=> 'form-control',
                        'placeholder' => 'Category Name',
                    ])}}
                     </div>
                    <div class="form-group">
                    {{ Form::label('name_en', 'Name(English):') }}
                    {{Form::text('name_en', $cat['name_en'],  [
                        'class'=> 'form-control',
                        'placeholder' => 'Category Name',
                    ])}}
                     </div>
                    <div class="form-group">
                    {{ Form::label('dd_code', 'Dewey Decimal Code:') }}
                    {{Form::text('dd_code', $cat['dd_code'],  [
                        'class'=> 'form-control',
                        'placeholder' => 'Category Name',
                    ])}}
                     </div>
                    <div class="form-group">
                    {{ Form::label('dd_name', 'Dewey Decimal Name:') }}
                    {{Form::text('dd_name', $cat['dd_name'],  [
                        'class'=> 'form-control',
                        'placeholder' => 'Category Name',
                    ])}}
                     </div>
                    <div class="form-group">
                    {{Form::submit('Update', [
                        'class' => 'btn btn-success margin-top-10',
                    ])}}
                    </div>

                    {{Form::close()}}

                </div>
            </div>
        </div>
    </div>
@stop
