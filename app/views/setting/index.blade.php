@extends('layouts.master')

@section('content')


    <div class="page-wrapper">


        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-gears fa-fw"></i>Backend Setting</h1>
            </div>

        </div>
        {{--end of header--}}

        <ul class="nav nav-tabs">
            <li class="active"><a href="#general" data-toggle="tab">General</a></li>
            <li><a href="#data" data-toggle="tab">Data</a></li>

        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="general">
                <h3>General</h3>
                {{ Form::open() }}
                <!-- Site Name Form Input-->

                <div class="form-group">
                    {{Form::label('Site Name','Site Name:')}}
                    {{Form::text('Site Name',null,['class' => 'form-control'])}}
                </div>

                <!-- Site Url Form Input-->

                <div class="form-group">
                    {{Form::label('Site Url','Site Url:')}}
                    {{Form::text('Site Url',null,['class' => 'form-control'])}}
                </div>
                <div class="form-group">
                    {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}
            </div>
            <div class="tab-pane" id="data">
                <h3>Backend Setting</h3>
                {{ Form::open() }}
                <!-- Data Form Input-->

                <div class="form-group">
                    {{Form::label('Data','Data:')}}
                    {{Form::text('Data',null,['class' => 'form-control'])}}
                </div>
                <div class="form-group">
                    {{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
                </div>
                {{ Form::close() }}
            </div>
        </div>

    </div>
@stop