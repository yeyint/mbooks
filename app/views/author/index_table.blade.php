<div class="table-responsiv">
    <table class="table table-bordered table-striped" id="dataTables-example">
        <thead>
        <tr>
            <td class="">ID</td>
            <td class="col-sm-2">Author Name</td>
            <td class="col-sm-2">Profile Pic</td>

            <td class="col-sm-2">Brief</td>
            <td class="">Actions</td>
        </tr>
        </thead>
        <tbody>
        @foreach($authors as $author)
            <tr class="gradeX">
                <td>{{$author['id']}}</td>
                <td>{{$author['name']}}</td>
                 <td>
                    <div class="row">
                        <div class="col-xs-12">
                            <?php $img =  URL::to('/'). '/'.Config::get('app.avatar_dir').'/' . $author['profile_pic'];  ?>
                            @if (is_file( public_path().'/'. Config::get('app.avatar_dir') . '/' . $author['profile_pic']))
                                <div class="profile_pic">
                                    <img src="{{ $img }}">
                                </div>
                            @else
                                -
                            @endif
                        </div>
                    </div>
                    Link: {{$author['profile_pic']}}
                </td>
                <td>{{$author['brief']}}</td>
                <td class="center">
                    <a class='btn btn-info btn-xs' href="{{route(Config::get('app.backend_url').'.author.edit', $author['id'])}}" title="Edit"><span class="glyphicon glyphicon-edit"></span> Edit</a>

                    @include('partials.modal', ['data' => $author, 'name' => 'author'])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@section('scripts')
<script>
    (function($) {
        $('.btn-delete').click(function(e) {
            e.preventDefault();
            var confirms = confirm('Are you really want to "DELETE"');
            if (!confirms) {
                return;
            }
            var id = $(this).data('id'),
                href = $(this).attr('href');
            $.ajax({
                url: href,
                method: 'DELETE',
                data: {},
                type: 'JSON'
            }).success(function(data) {
                if(data.success) {
                    window.location = window.location;
                }
            });
        });
    })(jQuery)
</script>
@stop
