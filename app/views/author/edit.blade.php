@extends('layouts.master')

@section('content')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-user fa-fw"></i>Edit Author

                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">

                    {{Form::open([
                        'url'    => route(Config::get('app.backend_url').'.author.update', $author['id']),
                        'role'   => 'form',
                        'class'  => 'col-lg-12 margin-bottom-40',
                        'method' => 'PUT',
                        'files'  => true
                    ])}}

                    {{Form::text('name', $author['name'],  [
                        'class'=> 'form-control margin-bottom-10',
                        'placeholder' => 'Author Name',
                    ])}}

                    {{-- profile pic --}}
                    <div class="form-group">
                        <label>Avatar</label>
                        {{ Form::file('avatar') }}
                    </div>

                    {{-- share_pic --}}
                    {{--<div class="form-group">
                        <label>Avatar</label>
                        {{ Form::file('share_pic') }}
                    </div>--}}
                    <div class="form-group">

                    {{Form::textarea('brief', $author['brief'],  [
                        'class'=> 'form-control margin-bottom-10',
                        'placeholder' => 'Biography',
                    ])}}
                    </div>
                    <div class="form-group">

                    {{Form::submit('Update', [
                        'class' => 'btn btn-success',
                    ])}}
                    </div>

                    {{Form::close()}}

                </div>
            </div>
        </div>
    </div>
@stop
