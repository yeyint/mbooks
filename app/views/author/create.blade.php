@extends('layouts.master')

@section('content')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-user fa-fw"></i>Add New Author
                <span> <a class="page-header" href="{{route(Config::get('app.backend_url').'.author.index')}}">Back</a></span></h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">

                    {{Form::open([
                        'url' => route(Config::get('app.backend_url').'.author.store'),
                        'role' => 'form',
                        'class' => 'col-lg-12',
                        'method' => 'POST',
                        'files' => true
                    ])}}
                    {{--1--}}
                    {{Form::text('name',null,  [
                        'class'=> 'form-control margin-bottom-10',
                        'placeholder' => 'Author Name',
                    ])}}


                    {{-- profile pic --}}
                    <div class="form-group">
                        <label>Avatar</label>
                        {{ Form::file('avatar') }}
                    </div>

                    {{-- share pic --}}
  {{--                  <div class="form-group">
                        <label>Share Pic</label>
                        {{ Form::file('share_pic') }}
                    </div>
--}}
                    {{--3--}}
                    <div class="form-group">
                    {{Form::textarea('brief',null,  [
                        'class'=> 'form-control margin-bottom-10',
                        'placeholder' => 'Biography',
                    ])}}
                    </div>
                    <div class="form-group">
                    {{--submit--}}
                    {{Form::submit('Add', [
                        'class' => 'btn btn-success',
                    ])}}
                    </div>

                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
@stop
