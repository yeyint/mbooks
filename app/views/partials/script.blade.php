
{{ HTML::script('components/jquery/dist/jquery.min.js') }}">
{{ HTML::script('components/bootstrap/dist/js/bootstrap.min.js') }}
{{ HTML::script('components/jquery-ui/jquery-ui.min.js') }}

<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-rc.2/js/select2.min.js"></script>
<!-- Sparkline -->
{{ HTML::script('adminlte/js/plugins/sparkline/jquery.sparkline.min.js') }}
<!-- jvectormap -->
{{ HTML::script('adminlte/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}
{{ HTML::script('adminlte/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}
<!-- jQuery Knob Chart -->
{{ HTML::script('adminlte/js/plugins/jqueryKnob/jquery.knob.js') }}
<!-- daterangepicker -->
{{ HTML::script('adminlte/js/plugins/daterangepicker/daterangepicker.js') }}
<!-- datepicker -->
{{ HTML::script('adminlte/js/plugins/datepicker/bootstrap-datepicker.js') }}
<!-- Bootstrap WYSIHTML5 -->
{{ HTML::script('adminlte/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}
<!-- iCheck -->
{{ HTML::script('adminlte/js/plugins/iCheck/icheck.min.js') }}

<!-- AdminLTE App -->
{{ HTML::script('adminlte/js/AdminLTE/app.js') }}

{{-- all scripts --}}
{{ HTML::script('js/all.js') }}

@section('script')

<script type="text/javascript">
  $('select').not('.book-category').select2();
  $('.book-category').select2({
    placeholder: "Choose category"
  });

  $('.book-tags').select2({
    tags: true,
    
  });
  
</script>