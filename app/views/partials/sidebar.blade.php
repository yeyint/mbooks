<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                 <img src="//www.gravatar.com/avatar/{{ md5(Sentry::getUser()->email)}}" alt="{{Sentry::getUser()->first_name}}" class="img-circle">
            </div>
            <div class="pull-left info">
                <p>{{Sentry::getUser()->first_name}}
                    {{Sentry::getUser()->last_name}}</p>



                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            {{ Form::open(['method' => 'GET'])}}
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i
                                class="fa fa-search"></i></button>
                    </span>
            </div>
            {{ Form::close() }}
            <!-- /.search form -->
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                @foreach(Config::get('backend.sidebar') as $menu)

                    @if(isset($menu['child']))
                <li class="treeview">
                    <a href="{{$menu['route']}}">
                        <i class="{{$menu['icon']}}"></i>
                        <span>{{$menu['label']}}</span>
                            <span class="fa pull-right fa-angle-left"></span>
                    </a>

                    <ul class="treeview-menu">

                        @foreach($menu['child'] as $child)
                        <li>

                            <a href="{{$child['route']}}" >
                                <i class="{{$child['icon']}}"></i>
                                {{$child['label']}}
                            </a>
                        </li>
                        @endforeach

                    </ul>

                    </li>
                    @else
                        <li class="">
                            <a href="{{$menu['route']}}">
                                <i class="{{$menu['icon']}}"></i>
                                <span>{{$menu['label']}}</span>

                            </a>

                        </li>

                    @endif
                    @endforeach
            </ul>


    </section>
    <!-- /.sidebar -->
</aside>