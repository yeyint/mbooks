
{{ HTML::style('components/bootstrap/dist/css/bootstrap.min.css') }}
{{ HTML::style('components/fontawesome/css/font-awesome.min.css') }}
<!-- Ionicons -->
<link rel="stylesheet" href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" type="text/css"/>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-rc.2/css/select2.min.css" rel="stylesheet" /><!-- Morris chart -->
{{ HTML::style('adminlte/css/morris/morris.css') }}
<!-- jvectormap -->
{{ HTML::style('adminlte/css/jvectormap/jquery-jvectormap-1.2.2.css') }}
<!-- Date Picker -->
{{ HTML::style('adminlte/css/datepicker/datepicker3.css') }}
<!-- Daterange picker -->
{{ HTML::style('adminlte/css/daterangepicker/daterangepicker-bs3.css') }}
<!-- bootstrap wysihtml5 - text editor -->
{{ HTML::style('adminlte/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}
<!-- Theme style -->
{{ HTML::style('adminlte/css/AdminLTE.css') }}

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
