@extends('layouts.master')

@section('content')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"> <i class="fa fa-cubes fa-fw"></i> Edit Sub_Category
                    <span> <a class="page-header" href="{{route(Config::get('app.backend_url').'.category.index')}}">Back</a></span>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="row">

                    {{Form::open([
                        'url' => route(Config::get('app.backend_url').'.subcategory.update', $subcategory['id']),
                        'role' => 'form',
                        'class' => 'col-lg-12',
                        'method' => 'PUT',
                    ])}}
                    <div class="form-group">
                    {{ Form::label('name_mm', 'Name(Myanmar):') }}
                    {{Form::text('name_mm', $subcategory['name_mm'],  [
                        'class'=> 'form-control',
                        'placeholder' => 'Category Name',
                    ])}}
                     </div>
                    <div class="form-group">
                    {{ Form::label('name_en', 'Name(English):') }}
                    {{Form::text('name_en', $subcategory['name_en'],  [
                        'class'=> 'form-control',
                        'placeholder' => 'Category Name',
                    ])}}
                     </div>
                     <div class="form-group">
                         {{ Form::label('main_category_id', 'Main Category:') }}
                         {{ Form::select('main_category_id',$category->getForSelect(),$subcategory['main_category_id'], ['class' => 'form-control margin-bottom-10']) }}
                       </div>
                    <div class="form-group">
                    {{ Form::label('dd_code', 'Dewey Decimal Code:') }}
                    {{Form::text('dd_code', $subcategory['dd_code'],  [
                        'class'=> 'form-control',
                        'placeholder' => 'Category Name',
                    ])}}
                     </div>
                    <div class="form-group">
                    {{ Form::label('dd_name', 'Dewey Decimal Name:') }}
                    {{Form::text('dd_name', $subcategory['dd_name'],  [
                        'class'=> 'form-control',
                        'placeholder' => 'Category Name',
                    ])}}
                     </div>
                    <div class="form-group">
                    {{Form::submit('Update', [
                        'class' => 'btn btn-success margin-top-10',
                    ])}}
                    </div>

                    {{Form::close()}}

                </div>
            </div>
        </div>
    </div>
@stop
