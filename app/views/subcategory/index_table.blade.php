<div class="table-responsive">
    <table class="table table-bordered table-striped" id="dataTables-example">
        <thead>
        <tr>
            <td class="">ID</td>
            <td class="">Name Myanmar</td>
            <td class="">Name English</td>
            <td class="">Main Category</td>
            <td class="">DD_Code</td>
            <td class="">DD_Name</td>
            <td class="">Actions</td>
        </tr>
        </thead>
        <tbody>
        @foreach($subcategory as $sub_cat)
            <tr class="gradeX">
                <td>{{$sub_cat['id']}}</td>
                <td>{{$sub_cat['name_mm']}}</td>
                <td>{{$sub_cat['name_en']}}</td>
                <td>{{$category->getName($sub_cat['main_category_id'])}}</td>
                <td>{{$sub_cat['dd_code']}}</td>
                <td>{{$sub_cat['dd_name']}}</td>
                <td class="center">

                    <a class='btn btn-info btn-xs' href="{{route(Config::get('app.backend_url').'.subcategory.edit', $sub_cat['id'])}}" title="Edit"><span class="glyphicon glyphicon-edit"></span> Edit</a>
{{--                    <a
                       data-id="{{$cat['id']}}"
                       href="{{route(Config::get('app.backend_url').'.subcategory.destroy', $cat['id'])}}"
                       class="fa btn-delete fa-trash action-btn" title="Delete"></a>--}}
                    @include('partials.modal', ['data' => $sub_cat, 'name' => 'subcategory'])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@section('scripts')
<script>
    (function($) {
        $('.btn-delete').click(function(e) {
            var confirms = confirm('Are you really want to "DELETE"');
            if (!confirms) {
                return;
            }
            e.preventDefault();
            var id = $(this).data('id'),
                href = $(this).attr('href');
            
            $.ajax({
                url: href,
                method: 'DELETE',
                data: {},
                type: 'JSON'
            }).success(function(data) {
                if(data.success) {
                    window.location = window.location;
                }
            });
        });
    })(jQuery)
</script>
@stop