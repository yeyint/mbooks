@extends('layouts.master')

@section('content')

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><i class="fa fa-cubes fa-fw"></i>  Sub_Category
                <a class="btn btn-success pull-right" href="{{route(Config::get('app.backend_url').'.subcategory.create')}}">

                <i class="fa fa-plus"></i>    Add New Subcategory</a>
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            {{-- Table --}}
            @include('subcategory.index_table')
            {{-- end of table --}}
        </div>
    </div>
</div>
@stop