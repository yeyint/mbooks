@extends('layouts.master')

@section('content')


    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><i class="fa fa-cubes fa-fw"></i>New Sub_Category
                    <span> <a class="page-header" href="{{route(Config::get('app.backend_url').'.category.index')}}">Back</a></span>
                </h1>


            </div>

            <!-- /.col-lg-12 -->
        </div>
        <div class="row">


                    {{Form::open([
                        'url' => route(Config::get('app.backend_url').'.subcategory.store'),
                        'role' => 'form',
                        'class' => 'col-lg-12',
                        'method' => 'POST',
                    ])}}

                       <div class="form-group">
                           {{ Form::label('name_mm', 'Name(Myanmar):') }}
                           {{ Form::text('name_mm', null, ['class' => 'form-control']) }}
                       </div>

                       <div class="form-group">
                           {{ Form::label('name_en', 'Name(English)') }}
                           {{ Form::text('name_en', null, ['class' => 'form-control']) }}
                       </div>
                       <div class="form-group">
                         {{ Form::label('main_category_id', 'Main Category:') }}
                         {{ Form::select('main_category_id',$category->getForSelect(),null, ['class' => 'form-control margin-bottom-10']) }}
                       </div>

                       <div class="form-group">
                           {{ Form::label('dd_code', 'Dewey Decimal Code') }}
                           {{ Form::text('dd_code', null, ['class' => 'form-control']) }}
                       </div>

                       <div class="form-group">
                              {{ Form::label('dd_name', 'Dewey Decimal Name') }}
                              {{ Form::text('dd_name', null, ['class' => 'form-control']) }}
                          </div>   
                       <div class="form-group">
                        {{Form::submit('Add', [
                            'class' => 'btn btn-success  ',
                        ])}}
                         </div>

                    {{Form::close()}}

        </div>
    </div>
@stop