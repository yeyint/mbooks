<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Library | @yield('title', 'Library')</title>
    <link rel="shortcut icon" type="image/png" href="{{ asset('img/book3.png') }}">
    @include('frontend.partials.meta')
    @include('frontend.partials.styles')
    @include('frontend.partials.scripts')
    @yield('headerstyles')
</head>
<body @yield('body-tags')>
    @yield('body')
<!-- ./mainwrapper -->

<!-- add new calendar event modal -->
@yield('footerscripts')
</body>
</html>
