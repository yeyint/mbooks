@extends('frontend.layouts.master')
@section('headerstyles')
{{ HTML::style('css/frontend.css') }}
@stop
@section('body')
<!-- Wrap all non-bar HTML in the .content div (this is actually what scrolls) -->
<div class="off-canvas-wrap" data-offcanvas>
{{-- <div> --}}
  <div class="inner-wrap">                
    {{-- ++++++ TOP BAR +++++++ --}}
    <nav class="top-bar show-for-medium-up" data-topbar role="navigation">
      <ul class="title-area">
        <li class="name">
          <h1 class="site-title"><a href="{{URL::to('/')}}">{{Config::get('app.name')}}</a></h1>
        </li>
        <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
        <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
      </ul>

      <section class="top-bar-section">
        <ul class="right">
          <li class="active"><a href="#">Right Button Active</a></li>
          <li class="has-dropdown">
            <a href="#">Right Button Dropdown</a>
            <ul class="dropdown">
              <li><a href="#">First link in dropdown</a></li>
              <li class="active"><a href="#">Active link in dropdown</a></li>
            </ul>
          </li>
        </ul>
      </section>
    </nav>
    {{-- ++++++ END OF TOP BAR +++++++ --}}

    {{-- TAB BAR --}}
    <div class="hide-for-medium-up">
      <nav class="tab-bar">
        <section class="left-small">
          <a class="left-off-canvas-toggle menu-icon" ><span></span></a>
        </section>
        <section class="middle tab-bar-section">
          <h1 class="titlen site-title">{{Config::get('app.name')}}</h1>
        </section>

      </nav>

      {{-- MENU --}}
      <aside class="left-off-canvas-menu">
        <ul class="off-canvas-list">
          <li><label>Menu</label></li>
          <li><a>link1</a></li>
          <li><a>link2</a></li>
          <li><a>link2</a></li>
        </ul>
      </aside>
    </div>


    {{-- Main Section --}}
    <section class="main-section">
    @include('frontend.pages.home-main-section')
    </section>


    <a class="exit-off-canvas"></a>
  </div><!--/innerWrapp-->
</div><!--/offCanvasWrap-->

<script>
  $(document).foundation();
  function viewport() {
    var e = window, a = 'inner';
    if (!('innerWidth' in window )) {
      a = 'client';
      e = document.documentElement || document.body;
    }
    return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
  }

  // When window resize off-canvas hide to right
  $(window).resize(function() {
    var width = viewport().width;
    if (width >= 641) {
      $('.off-canvas-wrap').foundation('offcanvas', 'hide', 'move-right');
    }
  });
</script>
@stop