 <div class="row books-row">
  <div class="books">
      @foreach ($books as $book)
        <div class="article item">
            <img class="img book-cover" src="{{$book->cover_pic}}">
               <div class="info">
                  <h6 class="title">{{$book->title}}</h6>
                  <p>{{$book->author_id}}<p5>
              </div>
            {{-- <a class="th [radius]" href="#">
            </a> --}}
            {{-- <span>
                  <span class="label [radius round]">Author: Nay Zaw Oo</span>
                  <span class="label alert [radius round]">Price: $200</span>
            </span> --}}
        </div>
      @endforeach
  </div>
 </div>

<script type="text/javascript">
  var wall = new freewall(".books");
      wall.reset({
        selector: '.item',
        animate: true,
        cellW: 200,
        cellH: 'auto',
        onResize: function() {
          wall.fitWidth();
        }
      });
      
      wall.container.find('.item img').load(function() {
        wall.fitWidth();
      });
</script>