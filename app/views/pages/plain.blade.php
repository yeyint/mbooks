<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>MyanmarBooks Activation</title>
	<link rel="shortcut icon" href="img/favicon.ico">
</head>
<body>
{{ $message or '' }}
@if(isset($alert_message))
<script>
	alert("{{$alert_message}}");
</script>
@endif

@if($close)
<script>
	window.close();
</script>
@endif
</body>
</html>