<?php

class Publisher extends Model {

    protected $table    = 'publishers';

    public static $rules=['name' => 'required'];

    protected static $messages = [
        'name.required' => 'Category name is require'
    ];

    protected $fillable = ['name','address','phone'];
    protected $hidden   = ['created_at','updated_at'];
    
    public function getName($id) {
      $name = $this->find($id);
      return ($name) ? $name->name : '-';
    }

    public function getForSelect ( )
    {  
     $_publisher = $this->all()->toArray();
     $publisher = [];
     foreach ($_publisher as $key) {
      $publisher[$key['id']] = $key['name'];
    }
    return $publisher;
  }
}
