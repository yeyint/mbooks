<?php

class Subcategory extends Model {

	protected $table    = 'sub_category';

    public static $rules=['name_mm' => 'required'];

    protected static $messages = [
        'name.required' => 'Category name is require'
    ];

    protected $fillable = ['name_mm','name_en','dd_code','dd_name','main_category_id'];
    protected $hidden   = ['created_at','updated_at'];
    
    public function getName($id) {
      $name = $this->find($id);
      return ($name) ? $name->name_mm : '-';
    }
    

}