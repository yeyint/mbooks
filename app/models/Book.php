<?php
/**
 * Created by PhpStorm.
 * User: yeyint
 * Date: 2/19/15
 * Time: 10:55 AM
 */



class Book extends Model {
    protected $table = 'books';
    protected $fillable = array(
        'unique_id',
        'title',
        'cover_pic',
        'share_pic',
        'subjects',
        'category_id',
        'author_id',
        'tag',
        'share_link',
        'is_notes',
        'notes',
        'physical_description',
        'isbn',
        'publisher_id',
        'review_at',
        'pages_no',
        'tag_id',
    );

    protected static $rules=['title' => 'required'];
    protected static $messages=['title.required' => 'Book title is require'];
    protected $hidden = ['created_at','updated_at'];

    public function hasByUniqueId($id) {
        if (empty($id))
            throw new Exception("ID must not be empty string.", 1);

        return !!($this->where('unique_id', $id)->first());
    }


    public function getForSelect ( )
    {
     $_book = $this->all()->toArray();


     $book = [];
     foreach ($_book as $key) {
      $book[$key['id']] = $key['title'];
    }
    return $book;
  }




}