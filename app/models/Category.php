<?php

class Category extends Model {

    protected $table    = 'category';

    public static $rules=['name_mm' => 'required'];

    protected static $messages = [
        'name.required' => 'Category name is require'
    ];

    protected $fillable = ['name_mm','name_en','dd_code','dd_name','custom_define_code'];
    protected $hidden   = ['created_at','updated_at'];
    
    public function getName($id) {
      $name = $this->find($id);
      return ($name) ? $name->name_mm : '-';
    }

    public function getNames($ids, $getByArray = false) {
        $ids = unserialize($ids);   
        $cats = $this->whereIn('id', $ids)->select('name_mm')->get()->toArray();       
        $cats = array_flatten($cats);
        if ($getByArray && $ids !=null) {

            return array_combine($ids, $cats);       
        }
        return (implode(', ', $cats));
    }

    public function getForSelect ()
    {  
  
     $_cats = $this->all()->toArray();
     $cats = [];
     foreach ($_cats as $key) {
      $cats[$key['id']] = $key['name_mm'];
    }
    return $cats;
  }
}

