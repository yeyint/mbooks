<?php

class Author extends Model {

    protected $table = 'author';
    protected $hidden = ['created_at','updated_at','extra'];
    protected $fillable = [
        'name', 'profile_pic', 'share_pic', 'brief',
    ];

    protected static $rules=['name' => 'required'];
    protected static $messages=['name.required' => 'Author name is require'];

    public function getName($id) {
        $name = $this->find($id);
        return ($name) ? $name->name : '-';
    }

    public function getForSelect ( )
    {
     $_author = $this->all()->toArray();
     $author = [];
     foreach ($_author as $key) {
      $author[$key['id']] = $key['name'];
    }
    return $author;
  }

}
