<?php
class Tag extends Model
{
    
    protected $fillable = ['name'];
    
    protected $table = 'tags';
    
    public function getForSelect() {
        
        $_tags = $this->all()->toArray();
        $tags = [];
        foreach ($_tags as $key) {
            $tags[$key['id']] = $key['name'];
        }
        return $tags;
    }
    
    public function getNames($ids, $getByArray = false) {
        $ids = unserialize($ids);
        
        $tags = $this->whereIn('id', $ids)->select('name')->get()->toArray();
        $tags = array_flatten($tags);
        if ($getByArray && $ids != null) {
            
            return array_combine($ids, $tags);
        }
        return $tags;
    }
    
    public function setNameAttribute($value) {
        $this->attributes['name'] = strtolower($value);
    }
}
