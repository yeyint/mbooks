<?php

class BaseModel extends Eloquent {
    public static function boot()
    {
        parent::boot();

        self::saving(function($model)
        {
            static::updateStatus($model->table);
        });
        self::creating(function($model)
        {
            static::updateStatus($model->table);
        });
        self::deleting(function($model)
        {   
            static::updateStatus($model->table);
        });
        self::updating(function($model)
        {
            static::updateStatus($model->table);
        });
    }

    public static function updateStatus( $table )
    { 
        $now = (new \Datetime)->getTimestamp();
        $history = History::where('table', $table)->first();
        if (!$history) {
            History::insert([
                'table'      => $table,
                'updatedat' => $now
                ]);
            return;
        }
        $history->update([
            'updatedat' => $now
            ]);
        return;
    }
}

?>
