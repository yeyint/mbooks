<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('books', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('unique_id', 20);
            $table->text('title');
            $table->text('cover_pic') ->nullable();
            $table->text('share_pic') ->nullable();
            $table->text('subjects');
            $table->string('category_id', 255);
            $table->string('author_id', 255);
            $table->string('publisher_id',255);
            $table->text('share_link');
            $table->text('notes');
            $table->boolean('is_notes');
            $table->string('physical_description');
            $table->string('isbn');
            $table->string('pages_no');
            $table->date('review_at');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('books');
	}

}
