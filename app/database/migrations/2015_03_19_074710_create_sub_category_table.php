<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sub_category', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name_mm');
			$table->string('name_en');
			$table->integer('dd_code');
			$table->string('dd_name');
			$table->integer('main_category_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sub_category');
	}

}
