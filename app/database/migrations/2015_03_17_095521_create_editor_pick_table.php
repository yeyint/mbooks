<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEditorPickTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('editor_pick', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('book_id');
			$table->string('book_title');
			$table->string('author_name');
			$table->string('book_subjects');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('editor_pick');
	}

}
