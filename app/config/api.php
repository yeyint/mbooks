<?php


return array(
        'mbook_wrapper'  => 'mbooks',
        'book_wrapper'    => 'books',
        'author_wrapper'   => 'authors',
        'category_wrapper' => 'categories',
        'publisher_wrapper' => 'publishers',
        'editorpick_wrapper'  => 'editorpicks', 

        'book_limit' => [
            'default'   => 30,
            'max'       => 300
        ],
        'author' => [                                            
            'id'          => 'author_id',
            'name'        => 'author_name',
            'profile_pic' => 'profile_picture',
            // 'share_pic'   => 'share_pic',
            'brief'       => 'biography',
        ],
        'category' => [                                        
            'id'        => 'category_id',
            'custom_define_code' => 'custom_define_code',
            'name_mm'   => 'category_name_mm',
            'name_en'   => 'category_name_en',
            'dd_code'   => 'dd_code',
            'dd_name'   => 'dd_name'
        ],
        'publisher' => [                                        
            'id'    => 'publisher_id',
            'name'  => 'publisher_name',
            'phone' => 'publisher_phone',
        ],
        'editorpick' => [                                        
            'id'    => 'editorpick_id',
            'book_title'  => 'book_title',
            'author_name' => 'author_id',
            'book_subjects' => 'book_subjects',
        ],
        'book' => [
            'unique_id'   => 'key',
            'title'     => 'title',
            'cover_pic'     => 'cover_pic',
            'subjects'     => 'subjects',
            'category_id' => 'categories_name',
            'author_id'   => 'author_name',
            'share_link' => 'share_link',
            'publisher_id' => 'publisher_name',
            'notes'     => 'notes',
            'physical_description' => 'physical_description',
            'isbn'           => 'isbn',
            'tag_id'    => 'tags_name',
            'related_books' => 'related_books',
            'authors_books' => 'authors_books',
        ],
    );


?>


