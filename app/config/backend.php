<?php

return array(
    'sidebar' => array(
        'book' => array(
            'icon'  => 'fa fa-book fa-fw',
            'label' => 'Book',
            'route' =>  route(Config::get('app.backend_url').'.book.index'),
        ),
        'categories' => array(
            'icon'  => 'fa fa-cube fa-fw',
            'label' => 'Category',
            'route' => route(Config::get('app.backend_url').'.category.index'),
        /*    'child' => array(
                'categories' => array(
                    'icon'  => 'fa fa-cube fa-fw',
                    'label' => 'Category',
                    'route' => route(Config::get('app.backend_url').'.category.index')),
                'subcategory' => array(
                    'icon'  => 'fa fa-cubes fa-fw',
                    'label' => 'Subcategory',
                    'route' => route(Config::get('app.backend_url').'.subcategory.index'),
                   

                ),  
                
            )*/
        ),

        'authors' => array(
            'icon'  => 'fa fa-user fa-fw',
            'label' => 'Author',
            'route' => route(Config::get('app.backend_url').'.author.index'),
        ),

        'editorpick'=> array(
            'icon'   => 'fa fa-file-text-o fa-fw',
            'label'  => 'Editor Pick',
            'route'  => route(Config::get('app.backend_url').'.editorpick.index'),
            ),
        'publisher'=> array(
            'icon'   => 'fa fa-shopping-cart fa-fw',
            'label'  => 'Publisher',
            'route'  => route(Config::get('app.backend_url').'.publisher.index'),
            ),

        'users' => array(
            'icon'  => 'fa fa-users fa-fw',
            'label' => 'Users',
            'route' => route(Config::get('app.backend_url').'.user.index'),
        ),
        'settings' => array(
            'icon'  => 'fa fa-gears fa-fw',
            'label' => 'Settings',
            'route' => '#',
            'child' => array(
                'dashboard' => array(
                    'icon'  => 'fa fa-gear fa-fw',
                    'label' => 'Site Setting',
                    'route' => route(Config::get('app.backend_url').'.setting.index'),
                )
            )
        )
    )


);


