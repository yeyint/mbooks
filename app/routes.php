<?php

//Validation
Route::pattern('id', '[0-9]+');
// Route::when('*', 'csrf', array('post', 'put'));

Route::get('some', function() {
   return URL::to('/');
});

// Website
Route::group(array(
        'before' => 'public'
    ), function() {
    Route::get('/', array(
        'as' => 'home',
        'uses' => 'FrontendController@home'
    ));
    
    // Route::get('/login', array(
    //     'as' => 'frontend.getLogin',
    //     'uses' => 'PublicUserAuthController@getLogin'
    // ));

    Route::get('/activation/{hashId}/{activationCode}', array(
        'as' => 'frontend.getAttemptActivation',
        'uses' => 'PublicUserAuthController@getAttemptActivation'
    ));

    Route::post('/apiauth', array(
        'as' => 'frontend.apiAuth',
        'uses' => 'PublicUserAuthController@apiAuth'
    ));

    // Route::post('/', array(
    //     'as' => 'frontend.postLogin',
    //     'uses' => 'PublicUserAuthController@postLogin'
    // ));    
});



  /*
  |--------------------------------------------------------------------------
  | Api 
  |--------------------------------------------------------------------------
  |
  | Api auth is close for local development 
  | for auth add filter 'auth.api'
  |
  */
Route::group(array('prefix' => 'api'), function() {
    
    Route::get('author',[
        'uses' => 'AuthorApiController@get'
    ]);
    Route::get('category',[
        'uses' => 'CategoryApiController@get'
    ]);
    Route::get('publisher',[
        'uses' => 'PublisherApiController@get'
    ]);
    Route::get('editorpick',[

      'uses' => 'EditorpickApiController@get'

    ]);


    Route::get('books',[
        'uses' => 'BookApiController@get'
    ]);

    Route::get('main', array(
        'as'   => 'QAC',
        'uses' => 'BookApiController@all'
    ));

    Route::get('update', function() {
        return History::get()->toArray();
    });

});




//Backend

Route::group(array('before' => 'guest', 'prefix' => Config::get('app.backend_url')), function() {
    Route::get('login', [
        'as'   => 'backend.getLogin',
        'uses' => 'AuthController@showLogin'
    ]);

    Route::post('login', [
        'as'  => 'backend.postLogin',
        'uses'=> 'AuthController@postLogin'
    ]);
});

Route::any('logout', [
    'as'    => 'logout',
    'uses'  => 'AuthController@logout'
]);

Route::group(array('before' => 'auth.admin', 'prefix' =>  Config::get('app.backend_url')), function() {

    Route::get('/', [
        'as'    => 'backend.home',
        'uses'  => 'BackendController@home'
    ]);

    Route::resource('setting','SettingController');

    Route::resource('category', 'CategoryController');
    Route::resource('author', 'AuthorController');
    Route::get('book/get_unique_id', 'BookBackendController@generateUniqueId');
    Route::resource('book', 'BookBackendController');
    Route::resource('user', 'UserController');
    Route::resource('editorpick','EditorpickController');
    Route::resource('review','ReviewController');
    Route::resource('publisher','PublisherController');
    Route::resource('subcategory','SubcategoryController');

    Route::get('fix', [
        'uses' => 'ApplicationController@fixTagsDatas'
    ]);

});





